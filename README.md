# PC-Builder
A small Python app made by @tahakazim08 and @HabibKhan1. We usually update the prices every month.

# To-do list:
## Decided:
- [X] CPUs
- [ ] CPU coolers
- [X] Motherboards
- [X] GPUs
- [ ] RAM
- [ ] SSDs
- [ ] Cases
- [ ] PSUs
- [ ] Monitors
- [X] In-app PC building guide
## Undecided:
- [ ] Add GUI
- [ ] Add tax

Of course, it's hard to keep track of everything, so we're mainly trying to add the mainstream PC parts.
gpu_am4_needed = True
gpu_lga_needed = True

####################
# CPU database

# Which Ryzen CPUs have integrated graphics? What are the prices? (Values of True and False tell whether or not the model has integrated graphics.)

Ryzen_3 = {
"3100" : [False, 99.00],
"3200g" : [True, 304.49],
"3300x" : [False, 149.00]  
}

Ryzen_5 = {
"3400g" : [True, 385.49],
"3600" : [False, 297.00],
"3600x" : [False, 249.00],
"5600g" : [True, 279.00],
"5600x" : [False, 259.00]
}

Ryzen_7 = {
"3700x" : [False, 399.00],
"5700g" : [True, 349.00],
"5800x" : [False, 399.00]
}

Ryzen_9 = {
"3900x" : [False, 546.79],
"3950x" : [False, 929.00],
"5900x" : [False, 539.98],
"5950x" : [False, 729.98]
}

AM4 = {"ryzen 3" : Ryzen_3, "ryzen 5" : Ryzen_5, "ryzen 7" : Ryzen_7, "ryzen 9" : Ryzen_9}

# Same thing for Intel

Core_i3 = {
"10100" : [True, 114.97],
"10100f" : [False, 87.99]
}

Core_i5 = {
"10400" : [True, 156.96],
"10400f" : [False, 154.79],
"11400" : [True, 189.88],
"11400f" : [False, 187.00],
"12600k" : [True, 289.00]
}

Core_i7 = {
"10700" : [True, 453.45],
"10700f" : [False, 279.99],
"12700k" : [True, 399.00]
}

Core_i9 = {
"10850k" : [True, 444.46],
"11900k" : [True, 439.86],
"12900k" : [True, 599.00]
}

LGA1700 = {"core i3" : Core_i3, "core i5" : Core_i5, "core i7" : Core_i7, "core i9" : Core_i9}

####################
# GPU database

# XT GPUs

XT = {
"5500" : 659.00, # It costs $659.00
"5600" : 739.00,
"5700" : 1099.00,
"6600" : 739.00,
"6700" : 899.00,
"6800" : 1739.00,
"6900" : 2099.00
}

# RX GPUs

RX = {
"6600" : 649.00
}

# GTX GPUs

GTX = {
"1050" : 299.00
}

# RTX GPUs

RTX = {
"2060" : 749.00,
"2070" : 1199.00
}

AMD_models = ["xt", "rx"]
NVidia_models = ["gtx", "rtx"]
GPU_brands = ["amd", "nvidia"]

####################
# Motherboard database

Asus = {
####################
# AM4 section
    
"crosshair vi hero" : 147.02, # Price: $147.02
"prime x470-pro" : 318.91,
"prime x570-pro" : 280.95,
"rog crosshair viii formula" : 736.49,
"rog crosshair viii hero" : 407.61

# I wanted to add LGA1700, but there weren't a lot of Asus motherboards supporting it.
}

MSI = {
####################
# AM4 section

"b350 gaming pro carbon" : 261.74,
"b550-a pro" : 134.99,
"mag b550 tomahawk" : 167.50,
"meg x570 ace" : 499.99,
"mpg b550 gaming carbon wifi" : 229.00,
"mpg b550 gaming edge wifi" : 230.47,

####################
# LGA1700 section

"mag z690 tomahawk wifi ddr4" : 299.99,
"mpg z690 edge wifi ddr4" : 319.99
}

motherboards = {"asus" : Asus, "msi" : MSI}

motherboard_brands = ["asus", "msi"]

# Ask for CPU model

def cpu_model(series, socket):

    if series.lower() not in AM4 and socket.lower() == "am4":
        print("Invalid series!")

        exit()
    if series.lower() not in LGA1700 and socket.lower() == "lga1700":
        print("Invalid series!")

        exit()

    if socket.lower() == "am4":
        cpu_model = input(f'Which {series.upper().replace("YZEN", "yzen")} model? (Type "L" to list them all.) ')
    else: # Must be LGA1700
        cpu_model = input(f'Which {series.upper().replace("ORE I", "ore i")} model? (Type "L" to list them all.) ')

    while cpu_model.lower() == "l":
        try:
            if socket.lower() == "am4":
                print(f'Available {series.upper().replace("YZEN", "yzen")} models:')
            else: # Must be LGA1700
                print(f'Available {series.upper().replace("ORE I", "ore i")} models:')

            if socket.lower() == "am4":
                for x in AM4[series.lower()]:
                    print(x)

                    if x == "real_name":
                        break
            else: # Must be LGA1700
                for x in LGA1700[series.lower()]:
                    print(x)

                    if x == "real_name":
                        break

            if socket.lower() == "am4":
                cpu_model = input(f'Which {series.upper().replace("YZEN", "yzen")} model? (Type "L" to list them all.) ')
            else: # Must be LGA1700
                cpu_model = input(f'Which {series.upper().replace("ORE I", "ore i")} model? (Type "L" to list them all.) ')
                
        except KeyboardInterrupt:
            print("Exiting....")

            exit()

    if socket.lower() == "am4":
        if cpu_model.lower() not in AM4[series.lower()]:
            print("Invalid CPU model!")

            exit()
    else:
        if cpu_model.lower() not in LGA1700[series.lower()]:
            print("Invalid CPU model!")

            exit()

    if socket.lower() == "am4":
        if AM4[series.lower()][cpu_model.lower()][0]: # Does it have integrated graphics?
            print("Your CPU has integrated graphics.")

            global gpu_am4_needed
            gpu_am4_needed = False
        else:
            print("Your CPU doesn't have integrated graphics.")
    else:
        if LGA1700[series.lower()][cpu_model.lower()][0]:
            print("Your CPU has integrated graphics.")

            global gpu_lga_needed
            gpu_lga_needed = False
        else:
            print("Your CPU doesn't have integrated graphics.")

    if socket.lower() == "am4":
        return AM4[series.lower()][cpu_model.lower()][1] # Return the cost
    else:
        return LGA1700[series.lower()][cpu_model.lower()][1]
    
# Ask for GPU

def get_gpu():
    if not gpu_am4_needed or not gpu_lga_needed:
        return 0
    
    brand = input("Which (GPU) brand? AMD or NVidia? ")

    while brand.lower() not in GPU_brands:
        try:
            print("Invalid brand!")
            brand = input("Which (GPU) brand? AMD or NVidia? ")
        except KeyboardInterrupt:
            print("Exiting....")

            exit()

    if brand.lower() == "amd":
        gpu_series = input("XT or RX? ")

        while gpu_series.lower() not in AMD_models:
            try:
                print("Invalid series!")
                gpu_series = input("XT or RX? ")
            except KeyboardInterrupt:
                print("Exiting....")

                exit()

        if gpu_series.lower() == "xt":
            gpu_model = input('Which XT model? (Type "L" to list them all.) ')

            while gpu_model.lower() == "l":
                try:
                    print("Available GPU models:\n")

                    for x in XT:
                        print(x)

                    gpu_model = input('Which XT model? (Type "L" to list them all.) ')
                except KeyboardInterrupt:
                    print("Exiting....")

                    exit()

            if gpu_model not in XT:
                print("Invalid GPU model!")

                exit()

            return XT[gpu_model] # Return the cost    
        else: # Must be RX
            gpu_model = input('Which RX model? (Type "L" to list them all.) ')

            while gpu_model.lower() == "l":
                try:
                    print("Available GPU models:\n")

                    for x in RX:
                        print(x)

                    gpu_model = input('Which XT model? (Type "L" to list them all.) ')
                except KeyboardInterrupt:
                    print("Exiting....")

                    exit()

            if gpu_model not in RX:
                print("Invalid GPU model!")

                exit()

            return RX[gpu_model] # Return the cost
    else: # Must be NVidia
        gpu_series = input("GTX or RTX? ")

        while gpu_series.lower() not in NVidia_models:
            try:
                print("Invalid series!")
                gpu_series = input("GTX or RTX? ")
            except KeyboardInterrupt:
                print("Exiting....")

                exit()

        if gpu_series.lower() == "gtx":
            gpu_model = input('Which GTX model? (Type "L" to list them all.) ')

            while gpu_model.lower() == "l":
                try:
                    print("Available GPU models:\n")

                    for x in GTX:
                        print(x)

                    gpu_model = input('Which GTX model? (Type "L" to list them all.) ')
                except KeyboardInterrupt:
                    print("Exiting....")

                    exit()

            if gpu_model not in GTX:
                print("Invalid GPU model!")

                exit()

            return GTX[gpu_model] # Return the cost
        else: # Must be RTX
            gpu_model = input('Which RTX model? (Type "L" to list them all.) ')

            while gpu_model.lower() == "l":
                try:
                    print("Available GPU models:\n")

                    for x in RTX:
                        print(x)

                    gpu_model = input('Which RTX model? (Type "L" to list them all.) ')
                except KeyboardInterrupt:
                    print("Exiting....")

                    exit()

            if gpu_model not in RTX:
                print("Invalid GPU model!")

                exit()

            return RTX[gpu_model] # Return the cost

# Ask for motherboard

def get_motherboard():
    motherboard_brand = input("Which motherboard brand? Asus or MSI? ")

    while motherboard_brand.lower() not in motherboard_brands:
        try:
            print("Invalid brand!")

            motherboard_brand = input("Which motherboard brand? Asus or MSI? ")
        except KeyboardInterrupt:
            print("Exiting....")

            exit()

    if motherboard_brand.lower() == "asus":
        motherboard_model = input('Which Asus model? (Type "L" to list them all.) ')

        while motherboard_model.lower() == "l":
            try:
                print("Available Asus motherboards:\n")

                for x in Asus:
                    print(x)

                motherboard_model = input('Which Asus model? (Type "L" to list them all.) ')
            except KeyboardInterrupt:
                print("Exiting....")

                exit()

        if motherboard_model.lower() not in Asus:
            print("Invalid motherboard!")

            exit()

        return Asus[motherboard_model.lower()] # Return the cost

    else: # Must be MSI
        motherboard_model = input('Which MSI model? (Type "L" to list them all.) ')

        while motherboard_model.lower() == "l":
            try:
                print("Available MSI motherboards:\n")

                for x in MSI:
                    print(x)

                motherboard_model = input('Which MSI model? (Type "L" to list them all.) ')
            except KeyboardInterrupt:
                print("Exiting....")

                exit()

        if motherboard_model.lower() not in MSI:
            print("Invalid motherboard!")

            exit()

        return MSI[motherboard_model.lower()] # Return the cost
